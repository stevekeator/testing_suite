@javascript
Feature: Basic Content Detection
  In order to read the correct content
  As a designnews.com user
  I need to be make sure the home page content loads properly

  Scenario: Searching for the content header
    Given I am visiting "www.designnews.com/"
    Then I should see "Automation"
