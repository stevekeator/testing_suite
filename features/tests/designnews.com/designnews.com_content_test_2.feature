@javascript
Feature: Admin User Login Check
  In order for admin users to log in
  As a designnews.com user
  I need to make sure admin users can log in correctly

  Scenario: Attempt to Log In
    Given I am visiting "www.designnews.com/"
    Given I am logged in to an SSO site as an admin user
    Then I should see "Hello Stephen"