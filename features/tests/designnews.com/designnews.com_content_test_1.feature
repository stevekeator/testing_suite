@javascript
Feature: Basic Login Check
  In order for users to log in
  As a designnews.com user
  I need to make sure users can log in correctly

  Scenario: Attempt to Log In
    Given I am visiting "www.designnews.com/"
    Given I am logged in to an SSO site as a user
    Then I should see "