@javascript
@designnews
Feature: Basic Login Check
  In order for users to log in
  As a designnews.com user
  I need to make sure users can see the correct content

  Scenario: Check the content
    Given I am on "/"
    Then the response should contain "Embedded Systems Designers Are Creating the Internet of Dangerous Things"