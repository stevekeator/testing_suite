<?php
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

class FeatureContext extends MinkContext implements Context, SnippetAcceptingContext {
  
  public function __construct($param1, $param2)
  {
    $this->param1 = $param1;
    $this->param2 = $param2;
  }
  
  /**
   * @Given /^I am visiting "([^"]*)"$/
   */
  public function iAmVisiting($arg1) {
    $this->setMinkParameter('base_url', $arg1);
  }
  
  /**
   * @Given /^I am logged in to an SSO site as a user$/
   */
  public function iAmLoggedInToAnSSOSiteAsAUser() {
    $abc = 123;
  }
    /* TODO: Convert coded process to featurized function
  }
    
  /**
   * @Given /^I am logged in to an SSO site as an admin user$/
   */
    public function iAmLoggedInToAnSSOSiteAsAnAdminUser() {
      $abc = 123;
    }
    /* TODO: Convert coded process to featurized function
  }

}