<?php
class AuthenticatedUser
{
  function loginUser($returnPage = '') {
    global $conf;
    global $session;
    global $page;
    
    $page = $session->getPage();
    
    // Click "Sign In" Button
    $signInButton = $page->find('xpath', '//*[@id="user_account"]/div[1]');
    $signInButton->click();
    
    // Switch to SSO IFRAME, refresh page object to load frame's contents
    $session->switchToIframe('iFrameResizer0');
    $page = $session->getPage();
    
    // Enter Username/Password Information
    $username_field = $page->find('xpath', '//*[@id="edit-name"]');
    $username_field->setValue($conf['user']['username']);
    $password_field = $page->find('xpath', '//*[@id="edit-pass"]');
    $password_field->setValue($conf['user']['password']);
    
    // Click the login button
    $loginButton = $page->find('xpath', '//*[@id="user-login"]/div');
    $loginButton->click();
    
    $returnPage == '' ? $returnPage = $conf['protocol'] . '://' . $conf['base_url'] : $returnPage = $returnPage;
    $session->visit($returnPage)->getPage();
  }
  
  
  function loginAdminUser($returnPage) {
    global $conf;
    global $session;
    global $page;
    
    $page = $session->getPage();
    
    // Enter Username/Password Information
    $username_field = $page->find('xpath', '//*[@id="edit-name"]');
    $username_field->setValue($conf['user']['admin_username']);
    $password_field = $page->find('xpath', '//*[@id="edit-pass"]');
    $password_field->setValue($conf['user']['admin_password']);
    
    $page->find('xpath', '//*[@id="edit-submit"]')->click();
    
    $returnPage == '' ? $returnPage = $conf['protocol'] . '://' . $conf['base_url'] : $returnPage = $returnPage;
    $session->visit($returnPage)->getPage();
  }
  
  function logoutUser() {
    global $session;
    $session->visit($conf['protocol'] . '://' . $conf['base_url'] . '/user/logout');
  }
}

?>