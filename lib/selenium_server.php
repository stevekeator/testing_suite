<?php

class SeleniumServer extends Execution {
  public function start() {
    $pid = $this->background('selenium-standalone start', 1);
    return $pid;
  }
  
  public function checkStatus() {
    return (bool) @fsockopen('127.0.0.1', '4444', $errno, $errstr, 0.1);
  }
}

$selenium_server = new SeleniumServer();
