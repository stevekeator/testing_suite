<?php

class UBMFeatures {
  function getFeatures() {
    global $conf;
    
    $features_list = array();
    
    // Scan the Features directory for our files
    $feature_file_list = scandir($conf['base_dir'] . '/features/tests');
    
    // Loop through files for our data
    foreach($feature_file_list as $file) {
      if (substr($file, -8) == '.feature') { // Only do this for .feature files
        array_push($features_list, UBMFeatures::getFeature($file));
      }
    }
    
    return $features_list;
  }
  
  public function getFeature($file) {
    global $conf;
    
    $keywords = new Behat\Gherkin\Keywords\ArrayKeywords(array(
      'en' => array(
        'feature'          => 'Feature',
        'background'       => 'Background',
        'scenario'         => 'Scenario',
        'scenario_outline' => 'Scenario Outline|Scenario Template',
        'examples'         => 'Examples|Scenarios',
        'given'            => 'Given',
        'when'             => 'When',
        'then'             => 'Then',
        'and'              => 'And',
        'but'              => 'But'
      )
    ));
  
    $lexer  = new Behat\Gherkin\Lexer($keywords);
    $parser = new Behat\Gherkin\Parser($lexer);
    
    $parsed = $parser->parse(file_get_contents($conf['base_dir'] . '/features/tests/' . $file));
    
    $parsed->filename = $file;
    
    return $parsed;
  }
  
}

$ubm_features = new UBMFeatures();