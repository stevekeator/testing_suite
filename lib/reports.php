<?php
class Reports {
  function strip_html($text) {
    $text = preg_replace(
      array(
        // Remove invisible content
        '@<head[^>]*?>.*?</head>@siu',
        '@<style[^>]*?>.*?</style>@siu',
        '@<script[^>]*?.*?</script>@siu',
        '@<object[^>]*?.*?</object>@siu',
        '@<embed[^>]*?.*?</embed>@siu',
        '@<applet[^>]*?.*?</applet>@siu',
        '@<noframes[^>]*?.*?</noframes>@siu',
        '@<noscript[^>]*?.*?</noscript>@siu',
        '@<noembed[^>]*?.*?</noembed>@siu',
        // Add line breaks before and after blocks
        '@</?((address)|(blockquote)|(center)|(del))@iu',
        '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
        '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
        '@</?((table)|(th)|(td)|(caption))@iu',
        '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
        '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
        '@</?((frameset)|(frame)|(iframe))@iu',
      ),
      array(
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
        "\n\$0", "\n\$0",
      ),
      $text );
    return strip_tags($text);
  }
  
  
  
  function createLog($type) {
    switch ($type) {
      case 'ad':
        $filename = 'ad_space_test__' . time() . '.log';
      case 'feature':
        $filename = 'feature__' . time() . '.log';
    }
    
    touch($conf['log_file_dir'] . '/' . $filename);
    return $logFileName;
  }
  
  function createBDDLog($timestamp) {
    $behat_report_file_dir = getcwd() . '/tmp/' . $timestamp . '/';
    
    $filelist = scandir($behat_report_file_dir);
    
    foreach($filelist as $file) {
      $file_parse = substr($file, 0, 4);
      switch ($file_parse) {
        // Behat report
        case 'Beha':
          // Open file, strip all tags for log
          $behat_report = file_get_contents(getcwd() . '/tmp/' . $timestamp . '/' . $file);
          $stripped_output = Reports::strip_html($behat_report);
          $log_output = '';
          foreach(preg_split("/((\r?\n)|(\r\n?))/", $stripped_output) as $line){
            if (trim($line) != '') {
              $log_output .= $line . chr(13) . chr(10);
            }
          }
          mkdir(getcwd() . '/log/bdd-' . $timestamp);
          // Write log and HTML report
          
          $logfile = file_put_contents(getcwd() . '/log/bdd-' . $timestamp . '/test_results.log', $log_output);
          $logfile_visual = file_put_contents(getcwd() . '/reports/bdd/' . $timestamp . '.results.html', $behat_report);
          continue;
        // HTML Report
        case 'Twig':
          $twig_report = file_get_contents(getcwd() . '/tmp/' . $timestamp . '/' . $file);
          $logfile_visual = file_put_contents(getcwd() . '/reports/bdd/' . $timestamp . '.report.html', $twig_report);
          continue;
      }
    }
    // File handling is done, cleanup temp directory
    $command = 'rm -fr ' . getcwd() . '/tmp/' . $timestamp;
    shell_exec($command);
  }
  
  
  function writeToLog($logname, $type, $message) {
    switch ($type) {
      case 'ad':
        //$file = file_put_contents()
    }
  }
  
  function getLogList() {
    global $conf;
    
    $logList = array();
    
    $logFileList = scandir(__DIR__ . $conf['log_file_dir']);
    
    foreach ($logFileList as $logFile) {
      $logFileSplit = explode($logFile, '.');
      $logFileList .= array('type' => $logFileSplit[0], 'date' => $logFileSplit[1]);
    }
    
    return $logFileList;
  }
  
  function getLogContents($logFileName) {
    global $conf;
    $log_contents = file_get_contents(__DIR__ . $conf['log_file_dir'] . '/' . $logFileName);
    
    foreach ($log_contents as $log_line) {
      
    }
  }
  

  
  
}


$reports = new Reports();