<?php
class Execution {
  public function background($Command, $Priority = 0){
    if($Priority)
      $PID = shell_exec("nohsup nice -n $Priority $Command > /dev/null & echo $! &");
    else
      $PID = shell_exec("nohup $Command > /dev/null & echo $! &");
    return($PID);
  }
  public function is_running($PID){
    exec("ps $PID", $ProcessState);
    return(count($ProcessState) >= 2);
  }
  public function kill($PID){
    if(exec::is_running($PID)){
      exec("kill -KILL $PID");
      return true;
    }else return false;
  }
  
  public function runBDDTest($filename) {
    // Set up Behat shell command and execute
    $report_build_time = time();
    $command = getcwd() . '/bin/behat ' . getcwd() . '/features/tests/' . $filename . ' --format html -c ' . getcwd() . '/behat.yml --format-settings=\'{"output_path": "tmp/' . $report_build_time . '"}\'';
    $output = shell_exec($command);
    
    // Get report and build summary
    call_user_func('Reports::createBDDLog', $report_build_time);
    return $report_build_time;
  }
  
  public function runBDDTests($filenames) {
    foreach($filenames as $filename) {
      $this->runBDDTest($filename);
    }
  }
  
};

$execution = new Execution();