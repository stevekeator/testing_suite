<?php

class ErrorHandler {
  public $errors_to_throw = array();
  
  private $errors = array(
    0 => 'Test Error, no problem.',
    1 => 'The Selenium server failed to start.  Please try running "selenium-standalone start" from the command line.',
    2 => 'The Selenium server failed to stop.',
  );
  
  
  function output() {
    if (sizeof($this->errors_to_throw) == 0) {
      return '';
    }
    else {
      $return_output = "<div class=\"alert alert-danger\"><h3><span class=\"glyphicon glyphicon-warning-sign\"></span> An error has occurred.</h3><ul>";
      foreach($this->errors_to_throw as $error) {
        $return_output .= "<li>" . $this->errors[$error] . "</li>";
      }
      $return_output .= "</ul></div>";
    }
    return $return_output;
  }
  
  function throw_error($errno) {
    array_push($this->errors_to_throw, $errno);
  }
  
  function clear_all() {
    $this->errors_to_throw = array();
  }
  
}

$error_handler = new ErrorHandler();
