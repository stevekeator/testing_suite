<?php

class Utils {
  function clearPopover($thePage, $xpathElement)
  {
    $elementToClick = $thePage->find('xpath', $xpathElement);
    $elementToClick->click();
  }
  
  function getElementCssValue($xpath)
  {
    $element = $page->find('xpath', $xpath);
    $style = $element->getAttribute('style');
    //if (preg_match("/(^{$property}:|; {$property}:) ([a-z0-9]+);/i", $style, $matches)) {
    var_dump($style);
    exit();
  }
  
  function widthScript($element_id, $sessionObject)
  {
    $jsStatement = "return document.getElementById(" . json_encode($element_id) . ").width;";
    return $sessionObject->evaluateScript($jsStatement);
  }
  
  function heightScript($element_id, $sessionObject)
  {
    $jsStatement = "return document.getElementById(" . json_encode($element_id) . ").heightf;";
    return $sessionObject->evaluateScript($jsStatement);
  }
  
  function assertElementHasCssValue($selector, $property, $value)
  {
    $page = $this->getSession()->getPage();
    $element = $page->find('css', $selector);
    if (empty($element)) {
      $message = sprintf('Could not find element using the selector "%s"', $selector);
      throw new \Exception($message);
    }
    $style = $this->elementHasCSSValue($element, $property, $value);
    if (empty($style)) {
      $message = sprintf('The property "%s" for the selector "%s" is not "%s"', $property, $selector, $value);
      throw new \Exception($message);
    }
  }
  
  function elementHasCSSValue($element, $property, $value)
  {
    $element_exists = FALSE;
    $style = $element->getAttribute('style');
    if ($style) {
      if (preg_match("/(^{$property}:|; {$property}:) ([a-z0-9]+);/i", $style, $matches)) {
        $found = array_pop($matches);
        if ($found == $value) {
          $element_exists = $element;
        }
      }
    }
    return $element_exists;
  }
  
  function strip_html_tags($str){
    $str = preg_replace('/(<|>)\1{2}/is', '', $str);
    $str = preg_replace(
        array(// Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            ),
        "", //replace above with nothing
        $str );
    $str = replaceWhitespace($str);
    $str = strip_tags($str);
    return $str;
  }
}

$utils = new Utils();
