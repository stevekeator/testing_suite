<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title; ?></title>
      <link rel="stylesheet" href="/vendor/components/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="/vendor/components/bootstrap-default/css/bootstrap.css" />
      <link rel="stylesheet" href="/template/css/testing-suite-specific.css" />
      <script type="text/javascript" src="/vendor/components/jquery/jquery.js"></script>
      <script type="text/javascript" src="/vendor/components/bootstrap/js/bootstrap.js"></script>
  </head>

  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <?php print $error_handler->output(); ?>
            </div>
            <div class="col-lg-3 col-lg-offset-3">
                <div class="alert <?php $check_selenium == 1 ? print 'alert-success' : print 'alert-danger'; ?>">
                    <div class="row">
                        <div class="col-xs-6">
                            Selenium Status: <br />
                            <strong><?php $check_selenium == true ? print 'Online' : print 'Offline'; ?></strong>
                        </div>
                        <div class="col-xs-6 text-right">
                          <?php if (!$check_selenium) { ?>
                              <a class="btn btn-warning" href="?startServer=1">Start Server</a>
                          <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>