<?php
/**
 * Created by PhpStorm.
 * User: skeator
 * Date: 3/14/17
 * Time: 11:00 AM
 */

$page_title = "Index";

include ('vendor/autoload.php');
include ('ad_space_config.php');
include('template/header.php');

global $check_selenium;

// Flags
if (isset($_GET['startServer'])) {
  $pid = $selenium_server->start();
  echo '<h5>' . $pid . '</h5>';
}

if (isset($_GET['runTest'])) {
  $log_time = $execution->runBDDTest($_GET['filename']);
}
?>

<style>
    .tab-pane {
        padding:10px;
    }
    
    .selected {
        background-color:#8888a9;
    }
</style>

<h1>Test Suite - REPO</h1>

<div>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation" class="active"><a href="#features-testing" aria-controls="features-testing" role="tab" data-toggle="tab">Features Testing</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Ad Testing</a></li>
    <li role="presentation"><a href="#settings" aria-controls="regression" role="tab" data-toggle="tab">Regression Testing</a></li>
    <li role="presentation" class="pull-right"><a href="#logs" aria-controls="logs" role="tab" data-toggle="tab">Logs</a></li>
    <li role="presentation" class="pull-right"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings <span class="glyphicon glyphicon-cog"></span></a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="home">...</div>
    <div role="tabpanel" class="tab-pane active" id="features-testing">
        <form name="featuresTableForm" method="post">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>Site</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Scenarios</th>
                    <th>Options</th>
                </tr>
              <?php
              // Get our features and output them
              $features_list = $ubm_features->getFeatures();
              $row = 0;
          
              foreach($features_list as $feature => $item) {
                ?>
                  <tr>
                      <td class="text-center"><input type="checkbox" name="feature_check[<?php echo $row; ?>]" value="<?php print $item->filename; ?>" class="multi-select" /></td>
                      <td>designnews.com</td>
                      <td><?php print $item->getTitle(); ?></td>
                      <td><?php print nl2br($item->getDescription()); ?></td>
                      <td>
                          <ul>
                            <?php
                            foreach($item->getScenarios() as $scenario) {
                              print '<li>' . $scenario->getTitle() . '</li>';
                            }
                            ?>
                          </ul>
                      </td>
                      <td>
                          <a class="btn btn-sm btn-success runTest" href="?runTest=1&filename=<?php print $item->filename; ?>"><span class="glyphicon glyphicon-play"></span> Run Test</a>
                          <a class="btn btn-sm btn-warning" href="edit.php?filename="><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                          <a class="btn btn-sm btn-danger" href="?deleteTest&filename="><span class="glyphicon glyphicon-trash"></span> Delete</a>
                      </td>
                  </tr>
            
                <?php
                $row++;
              }
              ?>
            </table>
            
        </form>
        <button class="btn btn-success runTest">Run Selected Tests</button>
        <button class="btn btn-warning">Delete Selected Tests</button>
        
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="regression">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div>
</div>

</div>

<?php
if (isset($log_time)) {
?>
    <div class="modal fade" tabindex="-1" role="dialog" id="resultModal">
        <div class="modal-dialog" role="document" style="width:95%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Test Results</h4>
                </div>
                <div class="modal-body" style="max-height:700px;">
                    <iframe id="testResults" src="reports/bdd/<?php echo $log_time; ?>.report.html?nocache=<?php echo time(); ?>" width="100%" height="640" frameBorder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="reports/bdd/<?php echo $log_time; ?>.results.html?nocache=<?php echo time(); ?>" target="_blank">Download Log</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  
    <script type="text/javascript">
        $(document).ready(function(){
            $('#resultModal').modal('show');
        });
    </script>
<?php
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="pleaseWaitModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Executing Test</h4>
            </div>
            <div class="modal-body text-center">
                    <h4>Test In Progress... Please Wait</h4>
                    <p>Depending on how many scenarios and steps there are in the test, your wait will vary.  This screen will
                    update when the results are available.</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    $(document).ready(function(){
        $('.runTest').on('click', function() {
            $('#pleaseWaitModal').modal('show');
        });
    });
</script>



<?php
include('template/footer.php');
?>
